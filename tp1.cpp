#include<string>
#include<iostream>
#include<iomanip>
#include<algorithm>
#include<getopt.h>
#include <fstream>
#include<set>

using namespace std;


/**
 * Remplis un set avec les mots du dictionnaire
 *
 * @param dictionnaireFichier   reference vers le fichier du dictionnaire
 * @param dictionnaire set contenant tous les mots du dictionnaire
 */
void remplirDico(ifstream &dictionnaireFichier, set <string> & dictionnaire){
  string motDico;

  do{
    getline(dictionnaireFichier, motDico);
    dictionnaire.insert(motDico);
  }while(!dictionnaireFichier.eof());
}

/**
 * Cherche la distance entre deux mots, utilise l'algoritme de Levenshtein
 *
 * @param mot   mot pris du fichier a corriger
 * @param corrige mot pris du dictionnaire auquel on compare mot
 * @param longueurMot longueur du string mot
 * @param longueurCor longueur du string corrige
 *
 *@returns cout de distance entre les deux mots
 */

int distance(string mot, string corrige, int longueurMot, int longueurCor){
  int matrice[longueurMot+1][longueurCor+1];

  for(int i =0; i<=longueurMot; i++){
    matrice[i][0] = i;
  }

  for(int j = 0; j<=longueurCor;j++){
    matrice[0][j] = j;
  }



  for(int i = 1; i<=longueurMot; i++){
    for(int j = 1; j<=longueurCor; j++){
      if(mot[i-1] == corrige[j-1]){
        matrice[i][j] = matrice[i-1][j-1];
      }else{
        matrice[i][j] = min({matrice[i-1][j] +1, matrice[i][j-1]+1, matrice[i-1][j-1] +1});
      }
    }
  }

  return matrice[longueurMot][longueurCor];
}

/**
 * cette methode sert a gerer le meilleur mot a garder selon la correction
 *
 * @param mot   mot pris du fichier a corriger
 * @param dictionnaire set contenant tous les mots du dictionnaire
 *
 * @returns le bon mot corrige
 */

string minErreur(string mot, set <string> &dictionnaire){
  //calculer le plus petit erreur pour optimisation
  string meilleurMot;
  int nbErreurs = 0, err;

  set <string> :: iterator it = next(dictionnaire.begin());

  while(it != dictionnaire.end()){

    if(nbErreurs == 0){

      nbErreurs = distance(mot, *it, mot.length(), (*it).length());
      meilleurMot = *it;

    }else{

      err = distance(mot, *it, mot.length(), (*it).length());

      if(nbErreurs > err){

        nbErreurs = err;
        meilleurMot = *it;

      }
    }

      if(nbErreurs == 1){
        return meilleurMot;
      }
    it = next(it);
  }

  return meilleurMot;
}

/**
 * cette methode sert a gerer le meilleur mot a garder selon la correction et choisis le
 * mot ayant la longueur la plus proche du mot de base
 *
 * @param mot   mot pris du fichier a corriger
 * @param dictionnaire set contenant tous les mots du dictionnaire
 *
 * @returns le bon mot corrige
 */

string semaS(string mot, set<string> &dictionnaire){
  string meilleurMot;
  int nbErreurs = 0, err, longueur, longueur2;

  set <string> :: iterator it = next(dictionnaire.begin());

  while(it != dictionnaire.end()){

    if(nbErreurs == 0){

      nbErreurs = distance(mot, *it, mot.length(), (*it).length());
      meilleurMot = *it;
      longueur = abs(mot.length() - (*it). length());
    }else{

      err = distance(mot, *it, mot.length(), (*it).length());
      longueur2 = abs(mot.length() - (*it). length());


      if(nbErreurs > err){
          nbErreurs = err;
          longueur = longueur2;
          meilleurMot = *it;

      }else if (nbErreurs == err){

        if(longueur2 < longueur){
          nbErreurs = err;
          longueur = longueur2;
          meilleurMot = *it;
        }
      }
    }
    it = next(it);
  }

  return meilleurMot;
}

/**
 * cette methode cherche si le mot existe dans le dictionnaire
 *
 *
 * @param mot   mot pris du fichier a corriger
 * @param dictionnaire set contenant tous les mots du dictionnaire
 *
 * @returns vrai si le mot existe, faux dans le cas contraire
 */

bool comparaison(string mot, set <string> &dictionnaire){

  set <string>::iterator it = next(dictionnaire.begin());

  while(it != dictionnaire.end()){
    while(mot.at(0) != (*it).at(0)){
      it = next(it);
    }

    if(mot.compare(*it) == 0){
      return true;
    }else{
      it = next(it);
      if (mot.at(0) != (*it).at(0)){
        return false;
      }
    }
  }

  return false;
}

/**
 * cette methode sert a mettre les caracteres en minuscules du mot a corriger
 *
 *
 * @param mot   mot pris du fichier a corriger
 *
 *
 * @returns le mot a corriger en minuscule
 */

string min(string mot){
  string motRet = mot;
  int i = 0;
  char c;

  while(i < motRet.length()){
    if(isupper(motRet[i])){
      motRet[i] = tolower(motRet[i]);
      i++;
    }else{
      i++;
    }
  }
  return motRet;
}


/**
 * cette methode sert a mettre les caracteres en majuscules selon le mot initial du mot corrige
 *
 *
 * @param motFaux   mot pris du fichier a corriger
 * @param motCorrige mot qui a ete pris du dictionnaire qui est la correction
 *
 * @returns le mot corrige avec la bonne typographie
 */
string maj(string motFaux, string motCorrige){
  string motRet = motCorrige;
  char c;
  c = motFaux[0];
  if(isupper(c)){
    motRet[0] = toupper(motCorrige[0]);
  }

  return motRet;
}

/**
 * cette methode lis les mots du fichier a corriger et appelle les methodes pour faire la correction
 *
 * @param fautesFichier reference vers le fichier a corriger
 * @param dictionnaire reference vers le set de mots du dictionnaire
 * @param s boolean qui verifie si nous utilisons la metrique secondaire ou pas
 */

void lectureMot(ifstream &fautesFichier, set <string> &dictionnaire, bool s){
  string mot;
  string motMin;
  char c;

  while(!fautesFichier.eof()){
    c = fautesFichier.get();

    if(isalpha(c) || c == '\''){

      while(isalpha(c) || c == '\''){
        mot += c;
        c = fautesFichier.get();
      }


      motMin = mot;

      motMin = min(motMin);



      if(comparaison(motMin, dictionnaire)){
        cout << mot;
      }else{
        if(!s){
          motMin = minErreur(motMin, dictionnaire);
          cout << maj(mot, motMin);
        }else{
          motMin = semaS(mot, dictionnaire);
          maj(mot, motMin);
          cout << motMin;
        }
      }
      mot.clear();

      if(!isalpha(c) || c != '\''){
        cout << c;
      }
    }else{
      cout << c ;
    }
  }
}

int main(int argc, char * argv[]) {
  char c;
  int options;
  string mot;
  mot.clear();
  bool s = false;
  ifstream dictionnaireFichier;
  ifstream fautesFichier;
  set <string> dictionnaire;


  dictionnaireFichier.open("words.txt");

  if(!dictionnaireFichier){
    cerr << "le dictionnaire n'a pas pu etre ouvert. Le programme va quitter." << endl;
    exit(-1);
  }

  while( (options = getopt(argc, argv, "s")) != -1 ){
    switch(options){

      case 's':
      s = true;
      break;

      default:
      break;
    }
  }

  string fichier = argv[optind];

  fautesFichier.open(fichier);

  if(!fautesFichier){
    cerr << "le fichier : "<<fichier<< " n'existe pas. Le programme va quitter." << endl;
    exit(-1);
  }

  remplirDico(dictionnaireFichier, dictionnaire);
  lectureMot(fautesFichier, dictionnaire, s);
  return 0;
}
